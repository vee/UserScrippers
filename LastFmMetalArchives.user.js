// ==UserScript==
// @name           last.fm metal archives buttons
// @namespace      https://github.com/Vendicated/LastFmMetalArchives
// @description    Add Metallum buttons to lastfm artists (revealed on hover)
// @match          https://www.last.fm/music/*
// @match          https://www.last.fm/*/music/*
// @match          https://www.last.fm/user/*
// @match          https://www.last.fm/*/user/*
// @grant          GM_addStyle
// ==/UserScript==

const re = /^\/music\/([^/#?\s]+)$/
const Selector = "a[href^='/music']:not([aria-hidden='true'])";
const HeaderSelector = ".header-new-title[itemprop='name']";

/**
 * @type {(node: HTMLElement, isHeader: boolean) => void}
 */
function addMetallumButton(node, isHeader) {
    let artist;
    if (isHeader) {
        artist = node.textContent;
    } else {
        const path = new URL(node.href).pathname;
        artist = re.exec(path)?.[1];
        if (!artist) return;
        artist = decodeURIComponent(artist);
    }

    const metallumElement = Object.assign(document.createElement("a"), {
        className: "v-metallum-btn",
        href: `https://www.metal-archives.com/search?type=band_name&searchString=${encodeURIComponent(artist)}`
    });

    const img = Object.assign(document.createElement("img"), {
        src: "https://www.metal-archives.com/favicon.ico",
        alt: `Search ${artist} on Metal Archives`
    })

    metallumElement.append(img);
    node.parentNode.prepend(metallumElement);
}

const observer = new MutationObserver(mutations => {
    for (const mut of mutations) {
        if (mut.type !== "childList") continue;

        for (const node of mut.addedNodes) {
            if (node.nodeType !== Node.ELEMENT_NODE) continue;
            node.querySelectorAll(Selector).forEach(n => addMetallumButton(n, false));
            node.querySelectorAll(HeaderSelector).forEach(n => addMetallumButton(n, true));
        }
    }
});

observer.observe(document.body, {
    childList: true,
    subtree: true,
})

document.querySelectorAll(Selector).forEach(n => addMetallumButton(n, false));
document.querySelectorAll(HeaderSelector).forEach(node => addMetallumButton(node, true));

GM_addStyle(`
.v-metallum-btn {
    display: none;
}

.v-metallum-btn img {
    width: auto;
    height: 100%;
    margin-right: 2px;
}

.v-metallum-btn + .grid-items-item-aux-block {
    display: inline-block;
}

.v-metallum-btn + .grid-items-item-aux-block + a {
    display: block;
}

:has(> .v-metallum-btn):hover .v-metallum-btn {
    display: inline-block;
}
`);