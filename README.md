# UserScrippers

## last.fm metal archives buttons [Download](LastFmMetalArchives.user.js)

Adds metal archives buttons (links) to artists everywhere. they are revealed on hover

![](screenshots/lastfmMetallum.png)
![](screenshots/lastfmMetallum2.png)
